FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/k6_practice*.jar
EXPOSE 8090
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]