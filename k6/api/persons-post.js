import http from "k6/http";
import { check } from 'k6';

const generateRandomString = function(length=6){
return Math.random().toString(20).substr(2, length)
}
const generateRandomNumber = function(length=2){
return Math.floor((Math.random() * 99) + 10);

}

export default function() {
  const url = 'http://localhost:8090/persons/';
  let headers = {'Content-Type': 'application/json'};
    let data = `{"name": "${generateRandomString()}",
                 "age":  "${generateRandomNumber()}"}`;


  let res = http.post(url, data, {headers: headers});
   check(res, {
        'is status 201': (r) => r.status === 201,
      });
};