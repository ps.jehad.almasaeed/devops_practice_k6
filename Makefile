message=""
run:
	java -jar -Dspring.profiles.active=h2 target/k6*.jar
post:
	k6 run -u 3 -i 20 -d 20s k6/persons-post.js --http-debug="full"
get:
	k6 run -u 2 -i 3 -d 20s k6/persons-get.js --http-debug="full"
curl:
	curl localhost:8090/persons/
jar:
	mvn clean package
git:
	git add .
	git commit -m "$(message)"
	git push